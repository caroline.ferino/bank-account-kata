package bank.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class Amount {

    private static final String VALUE_MUST_BE_POSITIVE_ERROR_MESSAGE = "Value of amount must be positive";
    private final BigDecimal value;

    public Amount(BigDecimal value) {
        if (value.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException(VALUE_MUST_BE_POSITIVE_ERROR_MESSAGE);
        }
        this.value = value.setScale(2, RoundingMode.HALF_EVEN);
    }

    public Amount(double value) {
        if (value < 0) {
            throw new IllegalArgumentException(VALUE_MUST_BE_POSITIVE_ERROR_MESSAGE);
        }
        this.value = new BigDecimal(value).setScale(2, RoundingMode.HALF_EVEN);
    }

    public BigDecimal getValue() {
        return value;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Amount)) return false;
        Amount amount = (Amount) o;
        return value.equals(amount.value);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(value);
    }
}
