package bank.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Objects;

public class Operation {

    private final OperationType operationType;
    private final Date creationDate;
    private final Amount amount;
    private final BigDecimal balance;

    public Operation(OperationType operationType, Amount amount, BigDecimal balance, Date creationDate) {
        this.operationType = operationType;
        this.amount = amount;
        this.balance = balance.setScale(2, RoundingMode.HALF_EVEN);
        this.creationDate = creationDate;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Amount getAmount() {
        return amount;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Operation)) return false;
        Operation operation = (Operation) o;
        return operationType.equals(operation.operationType) &&
                creationDate.equals(operation.creationDate) &&
                amount.equals(operation.amount) &&
                balance.equals(operation.balance);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(operationType, creationDate, amount, balance);
    }
}
