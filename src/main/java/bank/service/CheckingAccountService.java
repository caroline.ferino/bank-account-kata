package bank.service;

import bank.exception.CheckingAccountNotFoundException;
import bank.exception.WithdrawalNotPermittedException;
import bank.model.Amount;
import bank.model.Operation;
import bank.model.OperationType;
import bank.printer.IAccountStatementPrinter;
import bank.repository.IOperationRepository;

import java.math.BigDecimal;
import java.time.Clock;
import java.util.Date;
import java.util.List;

public class CheckingAccountService implements ICheckingAccountService, ICheckingAccountHistoryService {

    private static final String INSUFFICIENT_FUNDS_ERROR_MESSAGE = "Insufficient funds on the checking account";
    private final IOperationRepository operationRepository;
    private final Clock clock;

    public CheckingAccountService(IOperationRepository operationRepository, Clock clock) {
        this.operationRepository = operationRepository;
        this.clock = clock;
    }

    @Override
    public void makeDeposit(long accountId, Amount deposit) throws CheckingAccountNotFoundException {
        final BigDecimal newBalance = getAccountBalance(accountId).add(deposit.getValue());

        final Operation operation = new Operation(OperationType.DEPOSIT, deposit, newBalance, new Date(clock.millis()));

        operationRepository.add(accountId, operation);
    }

    @Override
    public void makeWithdrawal(long accountId, Amount withdrawal) throws CheckingAccountNotFoundException, WithdrawalNotPermittedException {
        final BigDecimal newBalance = getAccountBalance(accountId).subtract(withdrawal.getValue());

        if (newBalance.compareTo(BigDecimal.ZERO) < 0){
            throw new WithdrawalNotPermittedException(INSUFFICIENT_FUNDS_ERROR_MESSAGE);
        }

        final Operation operation = new Operation(OperationType.WITHDRAWAL, withdrawal, newBalance, new Date(clock.millis()));

        operationRepository.add(accountId, operation);
    }

    private BigDecimal getAccountBalance(long accountId) throws CheckingAccountNotFoundException {
        final List<Operation> operations = operationRepository.findAll(accountId);

        if (operations.size() <= 0) {
            return BigDecimal.ZERO;
        }

        final Operation lastOperation = operations.get(operations.size() - 1);

        return lastOperation.getBalance();
    }

    @Override
    public void printAccountStatement(long accountId, IAccountStatementPrinter IAccountStatementPrinter) throws CheckingAccountNotFoundException {
        final List<Operation> operations = operationRepository.findAll(accountId);

        IAccountStatementPrinter.print(accountId, operations);
    }
}
