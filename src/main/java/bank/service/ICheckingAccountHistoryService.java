package bank.service;

import bank.exception.CheckingAccountNotFoundException;
import bank.printer.IAccountStatementPrinter;

public interface ICheckingAccountHistoryService {
    void printAccountStatement(long accountId, IAccountStatementPrinter IAccountStatementPrinter) throws CheckingAccountNotFoundException;
}
