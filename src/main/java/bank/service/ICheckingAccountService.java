package bank.service;

import bank.exception.CheckingAccountNotFoundException;
import bank.exception.WithdrawalNotPermittedException;
import bank.model.Amount;

public interface ICheckingAccountService extends ICheckingAccountHistoryService {

    void makeDeposit(long accountId, Amount deposit) throws CheckingAccountNotFoundException;

    void makeWithdrawal(long accountId, Amount withdrawal) throws CheckingAccountNotFoundException, WithdrawalNotPermittedException;
}
