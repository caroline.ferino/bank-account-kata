package bank.repository;

import bank.exception.CheckingAccountNotFoundException;
import bank.model.Operation;

import java.util.List;

public interface IOperationRepository {

    void add(long accountId, Operation operation);

    List<Operation> findAll(long accountId) throws CheckingAccountNotFoundException;
}
