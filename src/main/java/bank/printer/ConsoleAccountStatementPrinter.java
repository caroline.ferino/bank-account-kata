package bank.printer;

import bank.model.Operation;

import java.text.SimpleDateFormat;
import java.util.List;

public class ConsoleAccountStatementPrinter implements IAccountStatementPrinter {

    private final String DATE_FORMAT = "dd/MM/yyyy HH:mm";

    @Override
    public void print(long accountId, List<Operation> operations) {
        System.out.println("Statements for account number " + accountId + " :");

        for (Operation operation : operations) {
            printOperation(operation);
        }
    }

    private void printOperation(Operation operation) {
        final String operationDescription = "Operation Type : " + operation.getOperationType().getName() +
                ", Creation date : " + new SimpleDateFormat(DATE_FORMAT).format(operation.getCreationDate()) +
                ", Amount : " + operation.getAmount().getValue() +
                ", Balance : " + operation.getBalance();

        System.out.println(operationDescription);
    }
}
