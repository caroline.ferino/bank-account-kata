package bank.printer;

import bank.model.Operation;

import java.util.List;

public interface IAccountStatementPrinter {
    void print(long accountId, List<Operation> operations);
}
