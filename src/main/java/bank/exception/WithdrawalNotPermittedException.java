package bank.exception;

public class WithdrawalNotPermittedException extends Exception {

    public WithdrawalNotPermittedException(String message) {
        super(message);
    }
}
