package bank.exception;

public class CheckingAccountNotFoundException extends Exception {

    public CheckingAccountNotFoundException(String message) {
        super(message);
    }
}
