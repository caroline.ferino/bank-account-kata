package printer;

import bank.model.Amount;
import bank.model.Operation;
import bank.model.OperationType;
import bank.printer.ConsoleAccountStatementPrinter;
import bank.printer.IAccountStatementPrinter;
import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ConsoleAccountStatementPrinterTests {

    private IAccountStatementPrinter accountStatementPrinter;
    private PrintStream formerOutputPrintStream;
    private ByteArrayOutputStream consoleOutputStream;

    private final String DATE_FORMAT = "dd/MM/yyyy HH:mm";

    @BeforeEach
    void set_up() {
        accountStatementPrinter = new ConsoleAccountStatementPrinter();

        consoleOutputStream = new ByteArrayOutputStream();
        formerOutputPrintStream = System.out;
        System.setOut(new PrintStream(consoleOutputStream));
    }

    @AfterEach
    void restoreOutputStream() {
        System.setOut(formerOutputPrintStream);
    }

    @Test
    void should_print_account_history() throws ParseException {

        final String expected = "Statements for account number 1 :" + System.lineSeparator() +
                "Operation Type : DEPOSIT, Creation date : 23/06/2019 09:35, Amount : 2500.00, Balance : 2500.00" + System.lineSeparator() +
                "Operation Type : DEPOSIT, Creation date : 24/06/2019 10:00, Amount : 250.25, Balance : 2750.25" + System.lineSeparator() +
                "Operation Type : WITHDRAWAL, Creation date : 24/06/2019 15:00, Amount : 50.00, Balance : 2700.25" + System.lineSeparator() +
                "Operation Type : DEPOSIT, Creation date : 25/06/2019 12:02, Amount : 25.00, Balance : 2725.25" + System.lineSeparator();

        final List<Operation> operations = new ArrayList<>();

        final Date firstDate = new SimpleDateFormat(DATE_FORMAT).parse("23/06/2019 09:35");
        final Operation firstOperation = new Operation(OperationType.DEPOSIT, new Amount(2500), new BigDecimal(2500), firstDate);
        operations.add(firstOperation);

        final Date secondDate = new SimpleDateFormat(DATE_FORMAT).parse("24/06/2019 10:00");
        final Operation secondOperation = new Operation(OperationType.DEPOSIT, new Amount(250.25), new BigDecimal(2750.25), secondDate);
        operations.add(secondOperation);

        final Date thirdDate = new SimpleDateFormat(DATE_FORMAT).parse("24/06/2019 15:00");
        final Operation thirdOperation = new Operation(OperationType.WITHDRAWAL, new Amount(50.00), new BigDecimal(2700.25), thirdDate);
        operations.add(thirdOperation);

        final Date fourthDate = new SimpleDateFormat(DATE_FORMAT).parse("25/06/2019 12:02");
        final Operation fourthOperation = new Operation(OperationType.DEPOSIT, new Amount(25.00), new BigDecimal(2725.25), fourthDate);
        operations.add(fourthOperation);

        accountStatementPrinter.print(1, operations);

        final String actual = consoleOutputStream.toString();

        assertEquals(expected, actual);
    }

    @Test
    void should_print_account_history_with_empty_account() {

        final String expected = "Statements for account number 1 :" + System.lineSeparator();

        final List<Operation> operations = new ArrayList<>();

        accountStatementPrinter.print(1, operations);

        final String actual = consoleOutputStream.toString();

        assertEquals(expected, actual);
    }

}
