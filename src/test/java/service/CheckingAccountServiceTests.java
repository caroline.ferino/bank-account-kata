package service;

import bank.exception.CheckingAccountNotFoundException;
import bank.exception.WithdrawalNotPermittedException;
import bank.model.Amount;
import bank.model.Operation;
import bank.model.OperationType;
import bank.printer.IAccountStatementPrinter;
import bank.repository.IOperationRepository;
import bank.service.CheckingAccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)

public class CheckingAccountServiceTests {

    @Mock
    private IOperationRepository operationRepositoryMock;

    @Mock
    private IAccountStatementPrinter accountStatementPrinterMock;

    private CheckingAccountService checkingAccountService;

    private final Clock fixedClock = Clock.fixed(Instant.parse("2019-06-24T16:45:42.00Z"), ZoneId.of("UTC"));
    private final String CHECKING_ACCOUNT_NOT_FOUND_ERROR_MESSAGE = "Checking account not found";
    private static final String INSUFFICIENT_FUNDS_ERROR_MESSAGE = "Insufficient funds on the checking account";

    @BeforeEach
    void set_up() {

        operationRepositoryMock = Mockito.mock(IOperationRepository.class);
        accountStatementPrinterMock = Mockito.mock(IAccountStatementPrinter.class);

        checkingAccountService = new CheckingAccountService(operationRepositoryMock, fixedClock);
    }

    @ParameterizedTest(name = "with operations from account {0}")
    @CsvSource({"1, 135.68", "2, 85.56", "3, 50.56"})
    void should_not_make_new_deposit_when_account_not_found(long accountId, BigDecimal value) throws CheckingAccountNotFoundException {
        final Amount inputAmount = new Amount(value);

        when(operationRepositoryMock.findAll(accountId)).thenThrow(new CheckingAccountNotFoundException(CHECKING_ACCOUNT_NOT_FOUND_ERROR_MESSAGE));

        final Throwable exception = assertThrows(CheckingAccountNotFoundException.class, () -> checkingAccountService.makeDeposit(accountId, inputAmount));
        assertEquals(CHECKING_ACCOUNT_NOT_FOUND_ERROR_MESSAGE, exception.getMessage());

        verify(operationRepositoryMock, times(1)).findAll(accountId);
        verifyNoMoreInteractions(operationRepositoryMock);
    }

    @ParameterizedTest(name = "with operations from account {0}")
    @CsvSource({"1, 135.68", "2, 85.56", "3, 50.56"})
    void should_make_new_deposit_with_empty_account(long accountId, BigDecimal value) throws CheckingAccountNotFoundException {
        final Amount inputAmount = new Amount(value);
        final Operation expectedOperation = new Operation(OperationType.DEPOSIT, inputAmount, inputAmount.getValue(), Date.from(fixedClock.instant()));

        when(operationRepositoryMock.findAll(accountId)).thenReturn(Collections.emptyList());

        checkingAccountService.makeDeposit(accountId, inputAmount);

        verify(operationRepositoryMock, times(1)).findAll(accountId);
        verify(operationRepositoryMock, times(1)).add(accountId, expectedOperation);
        verifyNoMoreInteractions(operationRepositoryMock);
    }

    @ParameterizedTest(name = "with operations from account {0}")
    @CsvSource({"1, 135.68, 25.63", "2, 85.56, 27", "3, 50.56, 125.8"})
    void should_make_new_deposit_with_existing_operations_account(long accountId, BigDecimal initialValue, BigDecimal deposit) throws CheckingAccountNotFoundException {
        final Amount initialAmount = new Amount(initialValue);
        final List<Operation> inputOperations = new ArrayList<>();
        inputOperations.add(new Operation(OperationType.DEPOSIT, initialAmount, initialAmount.getValue(), Date.from(fixedClock.instant())));

        final Amount depositAmount = new Amount(deposit);

        final BigDecimal expectedBalance = initialAmount.getValue().add(deposit);
        final Operation expectedOperation = new Operation(OperationType.DEPOSIT, depositAmount, expectedBalance, Date.from(fixedClock.instant()));

        when(operationRepositoryMock.findAll(accountId)).thenReturn(inputOperations);

        checkingAccountService.makeDeposit(accountId, depositAmount);

        verify(operationRepositoryMock, times(1)).findAll(accountId);
        verify(operationRepositoryMock, times(1)).add(accountId, expectedOperation);
        verifyNoMoreInteractions(operationRepositoryMock);
    }

    @ParameterizedTest(name = "with operations from id {0}")
    @CsvSource({"1, 34.56", "2, 15.56", "3, 50.56"})
    void should_not_make_new_withdrawal_when_account_not_found(long accountId, BigDecimal value) throws CheckingAccountNotFoundException {
        final Amount inputAmount = new Amount(value);

        when(operationRepositoryMock.findAll(accountId)).thenThrow(new CheckingAccountNotFoundException(CHECKING_ACCOUNT_NOT_FOUND_ERROR_MESSAGE));

        final Throwable exception = assertThrows(CheckingAccountNotFoundException.class, () -> checkingAccountService.makeWithdrawal(accountId, inputAmount));
        assertEquals(CHECKING_ACCOUNT_NOT_FOUND_ERROR_MESSAGE, exception.getMessage());

        verify(operationRepositoryMock, times(1)).findAll(accountId);
        verifyNoMoreInteractions(operationRepositoryMock);
    }

    @ParameterizedTest(name = "with operations from account {0}")
    @CsvSource({"1, 135.68", "2, 85.56", "3, 50.56"})
    void should_not_make_new_withdrawal_with_empty_account(long accountId, BigDecimal value) throws CheckingAccountNotFoundException {
        final Amount inputAmount = new Amount(value);

        when(operationRepositoryMock.findAll(accountId)).thenReturn(Collections.emptyList());

        final Throwable exception = assertThrows(WithdrawalNotPermittedException.class, () -> checkingAccountService.makeWithdrawal(accountId, inputAmount));
        assertEquals(INSUFFICIENT_FUNDS_ERROR_MESSAGE, exception.getMessage());

        verify(operationRepositoryMock, times(1)).findAll(accountId);
        verifyNoMoreInteractions(operationRepositoryMock);
    }

    @ParameterizedTest(name = "with operations from account {0}")
    @CsvSource({"1, 35.68, 125.63", "2, 85.56, 127", "3, 50.56, 125.8"})
    void should_not_make_new_withdrawal_when_not_enough_funds(long accountId, BigDecimal initialValue, BigDecimal withdrawal) throws CheckingAccountNotFoundException {
        final Amount initialAmount = new Amount(initialValue);
        final List<Operation> inputOperations = new ArrayList<>();
        inputOperations.add(new Operation(OperationType.DEPOSIT, initialAmount, initialAmount.getValue(), Date.from(fixedClock.instant())));

        final Amount withdrawalAmount = new Amount(withdrawal);

        when(operationRepositoryMock.findAll(accountId)).thenReturn(inputOperations);

        final Throwable exception = assertThrows(WithdrawalNotPermittedException.class, () -> checkingAccountService.makeWithdrawal(accountId, withdrawalAmount));
        assertEquals(INSUFFICIENT_FUNDS_ERROR_MESSAGE, exception.getMessage());

        verify(operationRepositoryMock, times(1)).findAll(accountId);
        verifyNoMoreInteractions(operationRepositoryMock);
    }

    @ParameterizedTest(name = "with operations from account {0}")
    @CsvSource({"1, 135.68, 25.63", "2, 85.56, 27", "3, 50.56, 25.8"})
    void should_make_new_withdrawal_when_enough_funds(long accountId, BigDecimal initialValue, BigDecimal withdrawal) throws CheckingAccountNotFoundException, WithdrawalNotPermittedException {
        final Amount initialAmount = new Amount(initialValue);
        final List<Operation> inputOperations = new ArrayList<>();
        inputOperations.add(new Operation(OperationType.DEPOSIT, initialAmount, initialAmount.getValue(), Date.from(fixedClock.instant())));

        final Amount withdrawalAmount = new Amount(withdrawal);

        final BigDecimal expectedBalance = initialAmount.getValue().subtract(withdrawal);
        final Operation expectedOperation = new Operation(OperationType.WITHDRAWAL, withdrawalAmount, expectedBalance, Date.from(fixedClock.instant()));

        when(operationRepositoryMock.findAll(accountId)).thenReturn(inputOperations);

        checkingAccountService.makeWithdrawal(accountId, withdrawalAmount);

        verify(operationRepositoryMock, times(1)).findAll(accountId);
        verify(operationRepositoryMock, times(1)).add(accountId, expectedOperation);
        verifyNoMoreInteractions(operationRepositoryMock);
    }

    @ParameterizedTest(name = "with operations from id {0}")
    @CsvSource({"1, 135.68", "2, 85.56", "3, 50.56"})
    void should_show_history(long accountId, BigDecimal value) throws CheckingAccountNotFoundException {
        final Amount inputAmount = new Amount(value);
        final List<Operation> inputOperations = new ArrayList<>();
        inputOperations.add(
                new Operation(OperationType.DEPOSIT, inputAmount, inputAmount.getValue(), Date.from(fixedClock.instant()))
        );

        when(operationRepositoryMock.findAll(accountId)).thenReturn(inputOperations);

        checkingAccountService.printAccountStatement(accountId, accountStatementPrinterMock);

        verify(operationRepositoryMock, times(1)).findAll(accountId);
        verify(accountStatementPrinterMock, times(1)).print(accountId, inputOperations);
        verifyNoMoreInteractions(operationRepositoryMock);
    }

}
