package model;

import bank.model.Amount;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class AmountTests {

    @Test
    void equals_contract() {
        EqualsVerifier.forClass(Amount.class)
                .withNonnullFields("value")
                .verify();
    }

    @ParameterizedTest
    @ValueSource(doubles = {-50, -50.25})
    void should_return_exception_if_negative_amount(double amount) {
        assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> new Amount(amount)),
                () -> assertThrows(IllegalArgumentException.class, () -> new Amount(BigDecimal.valueOf(amount)))
        );
    }

    @ParameterizedTest
    @ValueSource(doubles = {50, 50.25, 0})
    void should_instantiate_if_positive_amount(double amount) {
        final BigDecimal expected = new BigDecimal(amount).setScale(2, RoundingMode.HALF_EVEN);
        assertAll(
                () -> assertEquals(expected, new Amount(amount).getValue()),
                () -> assertEquals(expected, new Amount(expected).getValue())
        );
    }
}
