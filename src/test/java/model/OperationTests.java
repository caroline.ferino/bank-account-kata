package model;

import bank.model.Operation;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class OperationTests {

    @Test
    void equals_contract() {
        EqualsVerifier.forClass(Operation.class)
                .withNonnullFields("operationType", "creationDate", "amount", "balance")
                .verify();
    }
}
