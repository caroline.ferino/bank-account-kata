package integrationTests;

import bank.exception.CheckingAccountNotFoundException;
import bank.exception.WithdrawalNotPermittedException;
import bank.model.Amount;
import bank.model.Operation;
import bank.printer.ConsoleAccountStatementPrinter;
import bank.service.CheckingAccountService;
import bank.service.ICheckingAccountService;
import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class BankAccountIntegrationTests {

    private final Clock fixedClock = Clock.fixed(Instant.parse("2019-06-24T16:45:42.00Z"), ZoneId.of("UTC"));
    private PrintStream formerOutputPrintStream;
    private ByteArrayOutputStream consoleOutputStream;

    @BeforeEach
    void set_up() {
        consoleOutputStream = new ByteArrayOutputStream();
        formerOutputPrintStream = System.out;
        System.setOut(new PrintStream(consoleOutputStream));
    }

    @AfterEach
    void restoreOutputStream() {
        System.setOut(formerOutputPrintStream);
    }

    @Test
    void simple_scenario_test() throws CheckingAccountNotFoundException, WithdrawalNotPermittedException {

        final String expected = "Statements for account number 1 :" + System.lineSeparator() +
                "Operation Type : DEPOSIT, Creation date : 24/06/2019 18:45, Amount : 2500.00, Balance : 2500.00" + System.lineSeparator() +
                "Operation Type : DEPOSIT, Creation date : 24/06/2019 18:45, Amount : 250.00, Balance : 2750.00" + System.lineSeparator() +
                "Operation Type : WITHDRAWAL, Creation date : 24/06/2019 18:45, Amount : 500.00, Balance : 2250.00" + System.lineSeparator() +
                "Statements for account number 2 :" + System.lineSeparator() +
                "Operation Type : DEPOSIT, Creation date : 24/06/2019 18:45, Amount : 3000.50, Balance : 3000.50" + System.lineSeparator() +
                "Operation Type : DEPOSIT, Creation date : 24/06/2019 18:45, Amount : 15.25, Balance : 3015.75" + System.lineSeparator();

        final Map<Long, List<Operation>> accountOperations = new HashMap<>();
        accountOperations.put(1L, new ArrayList<>());
        accountOperations.put(2L, new ArrayList<>());

        final InMemoryOperationRepository operationRepository = new InMemoryOperationRepository(accountOperations);
        final ICheckingAccountService checkingAccountService = new CheckingAccountService(operationRepository, fixedClock);

        checkingAccountService.makeDeposit(1, new Amount(2500));
        checkingAccountService.makeDeposit(1, new Amount(250));
        checkingAccountService.makeWithdrawal(1, new Amount(500));

        checkingAccountService.makeDeposit(2, new Amount(3000.50));
        checkingAccountService.makeDeposit(2, new Amount(15.25));

        checkingAccountService.printAccountStatement(1, new ConsoleAccountStatementPrinter());
        checkingAccountService.printAccountStatement(2, new ConsoleAccountStatementPrinter());

        final String actual = consoleOutputStream.toString();

        assertEquals(expected, actual);
    }
}
