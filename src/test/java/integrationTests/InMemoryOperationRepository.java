package integrationTests;

import bank.exception.CheckingAccountNotFoundException;
import bank.model.Operation;
import bank.repository.IOperationRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class InMemoryOperationRepository implements IOperationRepository {
    private final Map<Long, List<Operation>> operations;
    private final String CHECKING_ACCOUNT_NOT_FOUND_ERROR_MESSAGE = "Checking account not found";

    public InMemoryOperationRepository(Map<Long, List<Operation>> operationList) {
        this.operations = operationList;
    }

    @Override
    public void add(long accountId, Operation operation) {
        if (operations.containsKey(accountId)) {
            operations.get(accountId).add(operation);
        } else {
            final List<Operation> operationList = new ArrayList<>();
            operationList.add(operation);
            operations.put(accountId, operationList);
        }
    }

    @Override
    public List<Operation> findAll(long accountId) throws CheckingAccountNotFoundException {
        if (!operations.containsKey(accountId)) {
            throw new CheckingAccountNotFoundException(CHECKING_ACCOUNT_NOT_FOUND_ERROR_MESSAGE);
        }

        return Collections.unmodifiableList(operations.get(accountId));
    }
}